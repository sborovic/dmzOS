set nobackup
set nowritebackup
set noswapfile
set autoread

set number relativenumber
autocmd BufWritePost *.[ch] exec "!indent -kr -nut % && rm %~"
